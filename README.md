# topN

Proposal:
Given an arbitrarily large file and a number, N, containing individual numbers on each line (e.g. 200Gb file), will output the largest N numbers, highest first.

Solution:
To accomplish this, I've just created a single thread program who is reading the file line per line with a buffered reader, and then storing the biggest numbers in a list that is shown to the user at the end.

Improvements:
It could be improved in the short future by:
 
- At the moment it only process Long type numbers, if needed it can be improved to process numbers with decimals

- Splitting the file if too big and running multiple threads with each file, then combining the results and print a subset


Run with:
mvn package && java -jar target/top-1.0-SNAPSHOT.jar