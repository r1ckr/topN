package com.richard.test;

import java.io.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class FileProcessor {

    private String filepath;
    private List<Long> topList = new LinkedList<Long>();

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public FileProcessor(String filepath, int top) {
        this.filepath = filepath;

        for (int i = 0; i < top ; i++) {
            topList.add(Long.valueOf(0));
        }
    }
    /**
     * Process the file
     * @return list with the biggest numbers of the file
     * */
    public List<Long> process() throws IOException {
        File numberListFile = new File(filepath);
        if (!numberListFile.exists()){
            // Return error
            throw new FileNotFoundException();
        }
        //Using buffered reader for big files
        BufferedReader br = new BufferedReader(new FileReader(numberListFile));

        //Reading the entire file
        String line;
        while((line = br.readLine()) != null) {
            if (!line.isEmpty())
                try {
                    insertIfBigger(Long.parseLong(line));
                }catch (NumberFormatException e){
                    System.out.println("Ignoring non number line: "+line);
                }

        }

        //Desc ordering
        Collections.reverse(topList);
        return topList;
    }

    /**
     * This method will insert the parameter number into the list if bigger than the current value in the position it beongs
     * @param number number to be compared with the top list*/
    private void insertIfBigger(Long number){
        for (int i = topList.size()-1; i >= 0; i--) {
            if(number.equals(topList.get(i))){
                break;
            }
            if (number > topList.get(i)){
                topList.add(i+1, number);
                topList.remove(0);
                break;
            }
        }
    }

}
