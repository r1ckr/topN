package com.richard.test;

import java.io.*;
import java.util.*;

/**
 * TopN main class!
 *
 */
public class Application
{

    public static void main( String[] args ) {

        if (args.length <= 1){
            //Show usage
            System.out.println("Usage");
            System.out.println("java -jar top path/to/filename/file.txt <top number>");
            System.exit(-1);
        }

        FileProcessor fileProcessor = new FileProcessor(args[0], Integer.parseInt(args[1]));

        try{
            System.out.println(Arrays.toString(fileProcessor.process().toArray()));
        }catch (FileNotFoundException e){
            System.out.println("File not found: " + args[0]);
        }catch (IOException e){
            System.out.println("Error reading the file: " + args[0]);
        }
    }



}
