package com.richard.test;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class FileProcessorTest {

    String filename = "test-numbers.txt";
    File testFile;
    Integer topNumber = 5;
    Integer maxNumber = 30000;

    @Before
    public void setup() throws Exception{

        List<Integer> intList = new LinkedList<Integer>();
        for (int i = 0; i <= maxNumber; i++) {
            intList.add(i);
        }

        long seed = System.nanoTime();
        Collections.shuffle(intList, new Random(seed));

        testFile = new File(filename);

        // if file doesnt exists, then create it
        if (!testFile.exists()) {
            testFile.createNewFile();
        }

        FileWriter fw = new FileWriter(filename);


        for (Integer number : intList){
            fw.write(number.toString()+"\r\n");
        }
        fw.close();

    }

    @Test
    public void testProcessor() throws Exception {

        FileProcessor fileProcessor = new FileProcessor(testFile.getAbsolutePath(),topNumber);
        List<Long> result = fileProcessor.process();

        for (int i = 0; i < 5 ; i++) {
            Assert.assertEquals(Long.valueOf(maxNumber-i),result.get(i));
        }
    }

    @Test(expected = FileNotFoundException.class)
    public void testFailProcessor() throws Exception {

        FileProcessor fileProcessor = new FileProcessor("fakefilename",topNumber);
        fileProcessor.process();

    }

    @After
    public void shutdown(){
        testFile.delete();
    }

}
